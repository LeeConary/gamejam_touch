using System.Text;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEditor;
using UnityEngine;

namespace DG.DOTweenEditor.UI
{
	[CustomEditor(typeof(DOTweenComponent))]
	public class DOTweenComponentInspector : Editor
	{
		private DOTweenSettings _settings;

		private readonly StringBuilder _strb = new StringBuilder();

		private bool _isRuntime;

		private Texture2D _headerImg;

		private string _playingTweensHex;

		private string _pausedTweensHex;

		private readonly GUIContent _gcPlay = new GUIContent("►");

		private readonly GUIContent _gcPause = new GUIContent("❚❚");

		private GUIContent _gcTitle;

		private GUIContent _gcDebugModeSuggest = new GUIContent("Activate both Safe Mode and Debug Mode (including all checkboxes) in DOTween's preferences in order to allow this Inspector to give you <b>way more information</b> (like using a red cross to mark tweens with NULL targets)");

		private void OnEnable()
		{
			_isRuntime = EditorApplication.isPlaying;
			ConnectToSource(true);
			_strb.Length = 0;
			_strb.Append("DOTween v").Append(DOTween.Version);
			if (TweenManager.isDebugBuild)
			{
				_strb.Append(" [Debug build]");
			}
			else
			{
				_strb.Append(" [Release build]");
			}
			if (EditorUtils.hasPro)
			{
				_strb.Append("\nDOTweenPro v").Append(EditorUtils.proVersion);
			}
			else
			{
				_strb.Append("\nDOTweenPro not installed");
			}
			if (EditorUtils.hasDOTweenTimeline)
			{
				_strb.Append("\nDOTweenTimeline v").Append(EditorUtils.dotweenTimelineVersion);
			}
			else
			{
				_strb.Append("\nDOTweenTimeline not installed");
			}
			_gcTitle = new GUIContent(_strb.ToString());
			_playingTweensHex = (EditorGUIUtility.isProSkin ? "<color=#00c514>" : "<color=#005408>");
			_pausedTweensHex = (EditorGUIUtility.isProSkin ? "<color=#ff832a>" : "<color=#873600>");
		}

		public override void OnInspectorGUI()
		{
			_isRuntime = EditorApplication.isPlaying;
			ConnectToSource();
			EditorGUIUtils.SetGUIStyles();
			GUILayout.Space(4f);
			GUILayout.BeginHorizontal();
			GUI.DrawTexture(GUILayoutUtility.GetRect(0f, 93f, 18f, 18f), _headerImg, ScaleMode.ScaleToFit, true);
			GUILayout.Label(_isRuntime ? "RUNTIME MODE" : "EDITOR MODE");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.Label(_gcTitle, TweenManager.isDebugBuild ? EditorGUIUtils.redLabelStyle : EditorGUIUtils.boldLabelStyle);
			if (!DOTween.useSafeMode || !DOTween.debugMode || !DOTween.debugStoreTargetId)
			{
				GUILayout.Label(_gcDebugModeSuggest, EditorGUIUtils.wordWrapRichTextLabelStyle);
			}
			if (!_isRuntime)
			{
				GUI.backgroundColor = new Color(0f, 0.31f, 0.48f);
				GUI.contentColor = Color.white;
				GUILayout.Label("This component is <b>added automatically</b> by DOTween at runtime.\nAdding it yourself is <b>not recommended</b> unless you really know what you're doing: you'll have to be sure it's <b>never destroyed</b> and that it's present <b>in every scene</b>.", EditorGUIUtils.infoboxStyle);
				GUI.backgroundColor = (GUI.contentColor = (GUI.contentColor = Color.white));
			}
			GUILayout.Space(6f);
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Documentation"))
			{
				Application.OpenURL("http://dotween.demigiant.com/documentation.php");
			}
			if (GUILayout.Button("Check Updates"))
			{
				Application.OpenURL("http://dotween.demigiant.com/download.php?v=" + DOTween.Version);
			}
			GUILayout.EndHorizontal();
			if (_isRuntime)
			{
				GUILayout.BeginHorizontal();
				if (GUILayout.Button("Play all"))
				{
					DOTween.PlayAll();
				}
				if (GUILayout.Button("Pause all"))
				{
					DOTween.PauseAll();
				}
				if (GUILayout.Button("Kill all"))
				{
					DOTween.KillAll();
				}
				GUILayout.EndHorizontal();
				int totActiveTweens = TweenManager.totActiveTweens;
				int num = TweenManager.TotalPlayingTweens();
				int totPausedTweens = totActiveTweens - num;
				int totActiveDefaultTweens = TweenManager.totActiveDefaultTweens;
				int totActiveLateTweens = TweenManager.totActiveLateTweens;
				int totActiveFixedTweens = TweenManager.totActiveFixedTweens;
				int totActiveManualTweens = TweenManager.totActiveManualTweens;
				GUILayout.Space(5f);
				_strb.Length = 0;
				_strb.Append("Active tweens: ").Append(totActiveTweens).Append(" (")
					.Append(TweenManager.totActiveTweeners)
					.Append(" TW, ")
					.Append(TweenManager.totActiveSequences)
					.Append(" SE)")
					.Append("\nDefault/Late/Fixed/Manual tweens: ")
					.Append(totActiveDefaultTweens)
					.Append("/")
					.Append(totActiveLateTweens)
					.Append("/")
					.Append(totActiveFixedTweens)
					.Append("/")
					.Append(totActiveManualTweens);
				GUILayout.Label(_strb.ToString(), EditorGUIUtils.wordWrapRichTextLabelStyle);
				GUILayout.Space(4f);
				DrawTweensButtons(num, totPausedTweens);
				GUILayout.Space(2f);
				_strb.Length = 0;
				_strb.Append("Pooled tweens: ").Append(TweenManager.TotalPooledTweens()).Append(" (")
					.Append(TweenManager.totPooledTweeners)
					.Append(" TW, ")
					.Append(TweenManager.totPooledSequences)
					.Append(" SE)");
				GUILayout.Label(_strb.ToString(), EditorGUIUtils.wordWrapRichTextLabelStyle);
				GUILayout.Space(2f);
				_strb.Remove(0, _strb.Length);
				_strb.Append("Tweens Capacity: ").Append(TweenManager.maxTweeners).Append(" TW, ")
					.Append(TweenManager.maxSequences)
					.Append(" SE")
					.Append("\nMax Simultaneous Active Tweens: ")
					.Append(DOTween.maxActiveTweenersReached)
					.Append(" TW, ")
					.Append(DOTween.maxActiveSequencesReached)
					.Append(" SE");
				GUILayout.Label(_strb.ToString(), EditorGUIUtils.wordWrapRichTextLabelStyle);
			}
			GUILayout.Space(8f);
			_strb.Remove(0, _strb.Length);
			_strb.Append("<b>SETTINGS ▼</b>");
			_strb.Append("\nSafe Mode: ").Append((_isRuntime ? DOTween.useSafeMode : _settings.useSafeMode) ? "ON" : "OFF");
			_strb.Append("\nLog Behaviour: ").Append(_isRuntime ? DOTween.logBehaviour : _settings.logBehaviour);
			_strb.Append("\nShow Unity Editor Report: ").Append(_isRuntime ? DOTween.showUnityEditorReport : _settings.showUnityEditorReport);
			_strb.Append("\nTimeScale (Unity/DOTween/DOTween-Unscaled): ").Append(Time.timeScale).Append("/")
				.Append(_isRuntime ? DOTween.timeScale : _settings.timeScale)
				.Append("/")
				.Append(_isRuntime ? DOTween.unscaledTimeScale : _settings.unscaledTimeScale);
			GUILayout.Label(_strb.ToString(), EditorGUIUtils.wordWrapRichTextLabelStyle);
			GUILayout.Label("NOTE: DOTween's TimeScale is not the same as Unity's Time.timeScale: it is actually multiplied by it except for tweens that are set to update independently", EditorGUIUtils.wordWrapRichTextLabelStyle);
			GUILayout.Space(8f);
			_strb.Remove(0, _strb.Length);
			_strb.Append("<b>DEFAULTS ▼</b>");
			_strb.Append("\ndefaultRecyclable: ").Append(_isRuntime ? DOTween.defaultRecyclable : _settings.defaultRecyclable);
			_strb.Append("\ndefaultUpdateType: ").Append(_isRuntime ? DOTween.defaultUpdateType : _settings.defaultUpdateType);
			_strb.Append("\ndefaultTSIndependent: ").Append(_isRuntime ? DOTween.defaultTimeScaleIndependent : _settings.defaultTimeScaleIndependent);
			_strb.Append("\ndefaultAutoKill: ").Append(_isRuntime ? DOTween.defaultAutoKill : _settings.defaultAutoKill);
			_strb.Append("\ndefaultAutoPlay: ").Append(_isRuntime ? DOTween.defaultAutoPlay : _settings.defaultAutoPlay);
			_strb.Append("\ndefaultEaseType: ").Append(_isRuntime ? DOTween.defaultEaseType : _settings.defaultEaseType);
			_strb.Append("\ndefaultLoopType: ").Append(_isRuntime ? DOTween.defaultLoopType : _settings.defaultLoopType);
			GUILayout.Label(_strb.ToString(), EditorGUIUtils.wordWrapRichTextLabelStyle);
			GUILayout.Space(10f);
		}

		private void ConnectToSource(bool forceReconnection = false)
		{
			_headerImg = AssetDatabase.LoadAssetAtPath(EditorUtils.editorADBDir + "Imgs/DOTweenIcon.png", typeof(Texture2D)) as Texture2D;
			if (_settings == null || forceReconnection)
			{
				_settings = (_isRuntime ? (Resources.Load("DOTweenSettings") as DOTweenSettings) : DOTweenUtilityWindow.GetDOTweenSettings());
			}
		}

		private void DrawTweensButtons(int totPlayingTweens, int totPausedTweens)
		{
			_strb.Length = 0;
			_strb.Append("Playing tweens: ").Append(totPlayingTweens);
			_settings.showPlayingTweens = EditorGUILayout.Foldout(_settings.showPlayingTweens, _strb.ToString());
			Tween[] activeTweens;
			if (_settings.showPlayingTweens)
			{
				activeTweens = TweenManager._activeTweens;
				foreach (Tween tween in activeTweens)
				{
					if (tween != null && tween.isPlaying)
					{
						DrawTweenButton(tween, true);
					}
				}
			}
			_strb.Length = 0;
			_strb.Append("Paused tweens: ").Append(totPausedTweens);
			_settings.showPausedTweens = EditorGUILayout.Foldout(_settings.showPausedTweens, _strb.ToString());
			if (!_settings.showPausedTweens)
			{
				return;
			}
			activeTweens = TweenManager._activeTweens;
			foreach (Tween tween2 in activeTweens)
			{
				if (tween2 != null && !tween2.isPlaying)
				{
					DrawTweenButton(tween2, false);
				}
			}
		}

		private void DrawTweenButton(Tween tween, bool isPlaying, bool isSequenced = false, int sequencedDepth = 0)
		{
			_strb.Length = 0;
			if (!isSequenced)
			{
				_strb.Append(isPlaying ? _playingTweensHex : _pausedTweensHex);
				_strb.Append(tween.isPlaying ? "► </color>" : "❚❚ </color>");
			}
			else
			{
				for (int i = 0; i < sequencedDepth; i++)
				{
					if (i == 0)
					{
						_strb.Append("         ");
					}
					else
					{
						_strb.Append("   ");
					}
				}
				_strb.Append("└ ");
			}
			if (tween.tweenType == TweenType.Sequence)
			{
				_strb.Append("[SEQUENCE] ");
			}
			AppendTweenIdLabel(_strb, tween);
			AppendDebugTargetIdLabel(_strb, tween);
			AppendTargetTypeLabel(_strb, tween.target);
			switch (tween.tweenType)
			{
			case TweenType.Tweener:
				if (!isSequenced)
				{
					GUILayout.BeginHorizontal();
					if (GUILayout.Button(isPlaying ? _gcPause : _gcPlay, EditorGUIUtils.btPlayPauseStyle))
					{
						if (isPlaying)
						{
							TweenManager.Pause(tween);
						}
						else
						{
							TweenManager.Play(tween);
						}
					}
				}
				if (GUILayout.Button(_strb.ToString(), isSequenced ? EditorGUIUtils.btSequencedStyle : EditorGUIUtils.btTweenStyle))
				{
					Object @object = tween.target as Object;
					if (@object != null)
					{
						EditorGUIUtility.PingObject(@object);
					}
				}
				if (!isSequenced)
				{
					GUILayout.EndHorizontal();
				}
				break;
			case TweenType.Sequence:
			{
				if (!isSequenced)
				{
					GUILayout.BeginHorizontal();
					if (GUILayout.Button(isPlaying ? _gcPause : _gcPlay, EditorGUIUtils.btPlayPauseStyle))
					{
						if (isPlaying)
						{
							TweenManager.Pause(tween);
						}
						else
						{
							TweenManager.Play(tween);
						}
					}
				}
				GUILayout.Button(_strb.ToString(), isSequenced ? EditorGUIUtils.btSequencedStyle : EditorGUIUtils.btSequenceStyle);
				if (!isSequenced)
				{
					GUILayout.EndHorizontal();
				}
				Sequence obj = (Sequence)tween;
				sequencedDepth++;
				foreach (Tween sequencedTween in obj.sequencedTweens)
				{
					DrawTweenButton(sequencedTween, isPlaying, true, sequencedDepth);
				}
				break;
			}
			}
		}

		private void AppendTweenIdLabel(StringBuilder strb, Tween t)
		{
			if (!string.IsNullOrEmpty(t.stringId))
			{
				strb.Append("ID:\"<b>").Append(t.stringId).Append("</b>\" ");
			}
			else if (t.intId != -999)
			{
				strb.Append("ID:\"<b>").Append(t.intId).Append("</b>\" ");
			}
			else if (t.id != null)
			{
				strb.Append("ID:\"<b>").Append(t.id).Append("</b>\" ");
			}
		}

		private void AppendDebugTargetIdLabel(StringBuilder strb, Tween t)
		{
			if (!string.IsNullOrEmpty(t.debugTargetId))
			{
				strb.Append("GO name: \"<b>").Append(t.debugTargetId).Append("</b>\"");
			}
		}

		private void AppendTargetTypeLabel(StringBuilder strb, object tweenTarget)
		{
			if (tweenTarget == null)
			{
				return;
			}
			strb.Append(' ');
			string text = tweenTarget.ToString();
			if (text == "null")
			{
				_strb.Append("<b><color=#ff0000>NULL TARGET</color></b>");
				return;
			}
			strb.Append("<i>(");
			int num = text.LastIndexOf('.');
			if (num == -1)
			{
				strb.Append(text).Append(')');
			}
			else
			{
				strb.Append(text.Substring(num + 1));
			}
			strb.Append("</i>");
		}
	}
}
