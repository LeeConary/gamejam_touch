using System;
using System.IO;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Core.Enums;
using UnityEditor;
using UnityEngine;

namespace DG.DOTweenEditor.UI
{
	public class DOTweenUtilityWindow : EditorWindow
	{
		private struct LocationData
		{
			public string dir;

			public string filePath;

			public string adbFilePath;

			public string adbParentDir;

			public LocationData(string srcDir)
			{
				this = default(LocationData);
				dir = srcDir;
				filePath = dir + EditorUtils.pathSlash + "DOTweenSettings.asset";
				adbFilePath = EditorUtils.FullPathToADBPath(filePath);
				adbParentDir = EditorUtils.FullPathToADBPath(dir.Substring(0, dir.LastIndexOf(EditorUtils.pathSlash)));
			}
		}

		private const string _Title = "DOTween Utility Panel";

		private static readonly Vector2 _WinSize = new Vector2(370f, 650f);

		public const string Id = "DOTweenVersion";

		public const string IdPro = "DOTweenProVersion";

		private static readonly float _HalfBtSize = _WinSize.x * 0.5f - 6f;

		private bool _initialized;

		private DOTweenSettings _src;

		private Texture2D _headerImg;

		private Texture2D _footerImg;

		private Vector2 _headerSize;

		private Vector2 _footerSize;

		private string _innerTitle;

		private bool _setupRequired;

		private Vector2 _scrollVal;

		private int _selectedTab;

		private string[] _tabLabels = new string[3] { "Setup", "Preferences", "Utilities" };

		private string[] _settingsLocation = new string[3] { "Assets > Resources", "DOTween > Resources", "Demigiant > Resources" };

		public static event Action<bool> OnRequestDOTweenProMissingScriptsFix;

		public static event Action<bool> OnRequestDOTweenTimelineMissingScriptsFix;

		private static void Dispatch_OnRequestDOTweenProMissingScriptsFix(bool currentSceneOnly)
		{
			if (DOTweenUtilityWindow.OnRequestDOTweenProMissingScriptsFix != null)
			{
				DOTweenUtilityWindow.OnRequestDOTweenProMissingScriptsFix(currentSceneOnly);
			}
		}

		private static void Dispatch_OnRequestDOTweenTimelineMissingScriptsFix(bool currentSceneOnly)
		{
			if (DOTweenUtilityWindow.OnRequestDOTweenTimelineMissingScriptsFix != null)
			{
				DOTweenUtilityWindow.OnRequestDOTweenTimelineMissingScriptsFix(currentSceneOnly);
			}
		}

		[MenuItem("Tools/Demigiant/DOTween Utility Panel")]
		private static void ShowWindow()
		{
			Open();
		}

		public static void Open()
		{
			EditorUtils.RetrieveDependenciesData(true);
			DOTweenUtilityWindow window = EditorWindow.GetWindow<DOTweenUtilityWindow>(true, "DOTween Utility Panel", true);
			window.minSize = _WinSize;
			window.maxSize = _WinSize;
			window.ShowUtility();
			EditorPrefs.SetString("DOTweenVersion", DOTween.Version);
			EditorPrefs.SetString("DOTweenProVersion", EditorUtils.proVersion);
		}

		private bool Init()
		{
			if (_initialized)
			{
				return true;
			}
			if (_headerImg == null)
			{
				_headerImg = AssetDatabase.LoadAssetAtPath(EditorUtils.editorADBDir + "Imgs/Header.jpg", typeof(Texture2D)) as Texture2D;
				if (_headerImg == null)
				{
					return false;
				}
				EditorUtils.SetEditorTexture(_headerImg, FilterMode.Bilinear, 512);
				_headerSize.x = _WinSize.x;
				_headerSize.y = (int)(_WinSize.x * (float)_headerImg.height / (float)_headerImg.width);
				_footerImg = AssetDatabase.LoadAssetAtPath(EditorUtils.editorADBDir + (EditorGUIUtility.isProSkin ? "Imgs/Footer.png" : "Imgs/Footer_dark.png"), typeof(Texture2D)) as Texture2D;
				EditorUtils.SetEditorTexture(_footerImg, FilterMode.Bilinear, 256);
				_footerSize.x = _WinSize.x;
				_footerSize.y = (int)(_WinSize.x * (float)_footerImg.height / (float)_footerImg.width);
			}
			_initialized = true;
			return true;
		}

		private void OnHierarchyChange()
		{
			Repaint();
		}

		private void OnEnable()
		{
			_innerTitle = "DOTween v" + DOTween.Version + (TweenManager.isDebugBuild ? " [Debug build]" : " [Release build]");
			if (EditorUtils.hasPro)
			{
				_innerTitle = _innerTitle + "\nDOTweenPro v" + EditorUtils.proVersion;
			}
			else
			{
				_innerTitle += "\nDOTweenPro not installed";
			}
			if (EditorUtils.hasDOTweenTimeline)
			{
				_innerTitle = _innerTitle + "\nDOTweenTimeline v" + EditorUtils.dotweenTimelineVersion;
			}
			else
			{
				_innerTitle += "\nDOTweenTimeline not installed";
			}
			Init();
			_setupRequired = EditorUtils.DOTweenSetupRequired();
		}

		private void OnDestroy()
		{
			if (_src != null)
			{
				_src.modules.showPanel = false;
				EditorUtility.SetDirty(_src);
			}
		}

		private void OnGUI()
		{
			if (!Init())
			{
				GUILayout.Space(8f);
				GUILayout.Label("Completing import process...");
				return;
			}
			Connect();
			EditorGUIUtils.SetGUIStyles(_footerSize);
			if (Application.isPlaying)
			{
				GUILayout.Space(40f);
				GUILayout.BeginHorizontal();
				GUILayout.Space(40f);
				GUILayout.Label("DOTween Utility Panel\nis disabled while in Play Mode", EditorGUIUtils.wrapCenterLabelStyle, GUILayout.ExpandWidth(true));
				GUILayout.Space(40f);
				GUILayout.EndHorizontal();
			}
			else
			{
				_scrollVal = GUILayout.BeginScrollView(_scrollVal);
				if (_src.modules.showPanel)
				{
					if (DOTweenUtilityWindowModules.Draw(this, _src))
					{
						_setupRequired = EditorUtils.DOTweenSetupRequired();
						_src.modules.showPanel = false;
						EditorUtility.SetDirty(_src);
					}
				}
				else
				{
					Rect rect = new Rect(0f, 0f, _headerSize.x, 30f);
					_selectedTab = GUI.Toolbar(rect, _selectedTab, _tabLabels);
					switch (_selectedTab)
					{
					case 1:
					{
						float labelWidth = EditorGUIUtility.labelWidth;
						EditorGUIUtility.labelWidth = 160f;
						DrawPreferencesGUI();
						EditorGUIUtility.labelWidth = labelWidth;
						break;
					}
					case 2:
						DrawUtilsGUI();
						break;
					default:
						DrawSetupGUI();
						break;
					}
				}
				GUILayout.EndScrollView();
			}
			if (GUI.changed)
			{
				EditorUtility.SetDirty(_src);
			}
		}

		private void DrawSetupGUI()
		{
			Rect rect = new Rect(0f, 30f, _headerSize.x, _headerSize.y);
			GUI.DrawTexture(rect, _headerImg, ScaleMode.StretchToFill, false);
			GUILayout.Space(rect.y + _headerSize.y + 2f);
			GUILayout.Label(_innerTitle, TweenManager.isDebugBuild ? EditorGUIUtils.redLabelStyle : EditorGUIUtils.boldLabelStyle);
			if (_setupRequired)
			{
				GUI.backgroundColor = Color.red;
				GUILayout.BeginVertical(GUI.skin.box);
				GUILayout.Label("DOTWEEN SETUP REQUIRED", EditorGUIUtils.setupLabelStyle);
				GUILayout.EndVertical();
				GUI.backgroundColor = Color.white;
			}
			else
			{
				GUILayout.Space(8f);
			}
			GUI.color = Color.green;
			using (new GUILayout.HorizontalScope())
			{
				GUILayout.FlexibleSpace();
				if (GUILayout.Button("<b>Setup DOTween...</b>\n(add/remove Modules)", EditorGUIUtils.btSetup, GUILayout.Width(200f)))
				{
					DOTweenUtilityWindowModules.ApplyModulesSettings();
					ASMDEFManager.ApplyASMDEFSettings();
					_src.modules.showPanel = true;
					EditorUtility.SetDirty(_src);
					EditorUtils.DeleteLegacyNoModulesDOTweenFiles();
					DOTweenDefines.RemoveAllLegacyDefines();
					EditorUtils.DeleteDOTweenUpgradeManagerFiles();
					return;
				}
				GUILayout.FlexibleSpace();
			}
			GUI.color = Color.white;
			GUILayout.Space(4f);
			using (new GUILayout.VerticalScope(GUI.skin.box))
			{
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.FlexibleSpace();
					GUI.color = (ASMDEFManager.hasModulesASMDEF ? Color.yellow : Color.cyan);
					if (GUILayout.Button(ASMDEFManager.hasModulesASMDEF ? "Remove ASMDEF..." : "Create ASMDEF...", EditorGUIUtils.btSetup, GUILayout.Width(200f)))
					{
						if (ASMDEFManager.hasModulesASMDEF)
						{
							string text = "This will remove:\n-DOTween/Modules/DOTween.Modules.asmdef";
							if (EditorUtils.hasPro)
							{
								text += "\n-DOTweenPro/DOTweenPro.Scripts.asmdef\n-DOTweenPro/Editor/DOTweenPro.EditorScripts.asmdef";
							}
							if (EditorUtils.hasDOTweenTimeline)
							{
								text += "\n-DOTweenTimeline/Scripts/DOTweenTimeline.Scripts.asmdef\n-DOTweenTimeline/Scripts/Editor/DOTweenTimeline.EditorScripts.asmdef";
							}
							if (EditorUtility.DisplayDialog("Remove ASMDEF", text, "Ok", "Cancel"))
							{
								ASMDEFManager.RemoveAllASMDEF();
							}
						}
						else
						{
							string text2 = "This will create:\n-DOTween/Modules/DOTween.Modules.asmdef";
							if (EditorUtils.hasPro)
							{
								text2 += "\n-DOTweenPro/DOTweenPro.Scripts.asmdef\n-DOTweenPro/Editor/DOTweenPro.EditorScripts.asmdef";
							}
							if (EditorUtils.hasDOTweenTimeline)
							{
								text2 += "\n-DOTweenTimeline/Scripts/DOTweenTimeline.Scripts.asmdef\n-DOTweenTimeline/Scripts/Editor/DOTweenTimeline.EditorScripts.asmdef";
							}
							if (EditorUtility.DisplayDialog("Create ASMDEF", text2, "Ok", "Cancel"))
							{
								ASMDEFManager.CreateAllASMDEF();
							}
						}
					}
					GUI.color = Color.white;
					GUILayout.FlexibleSpace();
				}
				GUILayout.Label("ASMDEFs are useful if you need to reference the extra DOTween modules API (like [<i>UIelement</i>].DOColor) from other ASMDEFs/Libraries instead of loose scripts, but remember to have those <b>ASMDEFs/Libraries reference DOTween ones</b>, <b>except for DOTween's Editor ASMDEFs</b> (DOTweenPro.EditorScripts) which <b>must never be referenced</b> by your runtime code or runtime ASMDEFs.", EditorGUIUtils.wordWrapRichTextLabelStyle);
			}
			GUILayout.Space(3f);
			using (new GUILayout.HorizontalScope())
			{
				if (GUILayout.Button("Website", EditorGUIUtils.btBigStyle, GUILayout.Width(_HalfBtSize)))
				{
					Application.OpenURL("http://dotween.demigiant.com/index.php");
				}
				if (GUILayout.Button("Get Started", EditorGUIUtils.btBigStyle, GUILayout.Width(_HalfBtSize)))
				{
					Application.OpenURL("http://dotween.demigiant.com/getstarted.php");
				}
			}
			using (new GUILayout.HorizontalScope())
			{
				if (GUILayout.Button("Documentation", EditorGUIUtils.btBigStyle, GUILayout.Width(_HalfBtSize)))
				{
					Application.OpenURL("http://dotween.demigiant.com/documentation.php");
				}
				if (GUILayout.Button("Support", EditorGUIUtils.btBigStyle, GUILayout.Width(_HalfBtSize)))
				{
					Application.OpenURL("http://dotween.demigiant.com/support.php");
				}
			}
			using (new GUILayout.HorizontalScope())
			{
				if (GUILayout.Button("Changelog", EditorGUIUtils.btBigStyle, GUILayout.Width(_HalfBtSize)))
				{
					Application.OpenURL("http://dotween.demigiant.com/download.php");
				}
				if (GUILayout.Button("Check Updates", EditorGUIUtils.btBigStyle, GUILayout.Width(_HalfBtSize)))
				{
					Application.OpenURL("http://dotween.demigiant.com/download.php?v=" + DOTween.Version);
				}
			}
			GUILayout.Space(4f);
			if (GUILayout.Button(_footerImg, EditorGUIUtils.btImgStyle))
			{
				Application.OpenURL("http://www.demigiant.com/");
			}
		}

		private void DrawPreferencesGUI()
		{
			GUILayout.Space(40f);
			if (GUILayout.Button("Reset", EditorGUIUtils.btBigStyle))
			{
				_src.useSafeMode = true;
				_src.safeModeOptions.logBehaviour = SafeModeLogBehaviour.Warning;
				_src.safeModeOptions.nestedTweenFailureBehaviour = NestedTweenFailureBehaviour.TryToPreserveSequence;
				_src.showUnityEditorReport = false;
				_src.timeScale = 1f;
				_src.unscaledTimeScale = 1f;
				_src.useSmoothDeltaTime = false;
				_src.maxSmoothUnscaledTime = 0.15f;
				_src.rewindCallbackMode = RewindCallbackMode.FireIfPositionChanged;
				_src.logBehaviour = LogBehaviour.ErrorsOnly;
				_src.drawGizmos = true;
				_src.defaultRecyclable = false;
				_src.defaultAutoPlay = AutoPlay.All;
				_src.defaultUpdateType = UpdateType.Normal;
				_src.defaultTimeScaleIndependent = false;
				_src.defaultEaseType = Ease.OutQuad;
				_src.defaultEaseOvershootOrAmplitude = 1.70158f;
				_src.defaultEasePeriod = 0f;
				_src.defaultAutoKill = true;
				_src.defaultLoopType = LoopType.Restart;
				_src.debugMode = false;
				_src.debugStoreTargetId = true;
				EditorUtility.SetDirty(_src);
			}
			GUILayout.Space(8f);
			_src.useSafeMode = EditorGUILayout.Toggle("Safe Mode", _src.useSafeMode);
			if (_src.useSafeMode)
			{
				_src.safeModeOptions.logBehaviour = (SafeModeLogBehaviour)(object)EditorGUILayout.EnumPopup(new GUIContent("└ Log Type On Capture", "Type of log to throw when safe mode captures an error"), _src.safeModeOptions.logBehaviour);
				_src.safeModeOptions.nestedTweenFailureBehaviour = (NestedTweenFailureBehaviour)(object)EditorGUILayout.EnumPopup(new GUIContent("└ On Nested Tween Failure", "Behaviour in case a tween inside a Sequence fails"), _src.safeModeOptions.nestedTweenFailureBehaviour);
			}
			_src.timeScale = EditorGUILayout.FloatField("DOTween's TimeScale", _src.timeScale);
			_src.unscaledTimeScale = EditorGUILayout.FloatField("└ Unscaled TimeScale", _src.unscaledTimeScale);
			_src.useSmoothDeltaTime = EditorGUILayout.Toggle("Smooth DeltaTime", _src.useSmoothDeltaTime);
			_src.maxSmoothUnscaledTime = EditorGUILayout.Slider("Max SmoothUnscaledTime", _src.maxSmoothUnscaledTime, 0.01f, 1f);
			_src.rewindCallbackMode = (RewindCallbackMode)(object)EditorGUILayout.EnumPopup("OnRewind Callback Mode", _src.rewindCallbackMode);
			GUILayout.Space(-5f);
			GUILayout.BeginHorizontal();
			GUILayout.Space(EditorGUIUtility.labelWidth + 4f);
			EditorGUILayout.HelpBox((_src.rewindCallbackMode == RewindCallbackMode.FireIfPositionChanged) ? "When calling Rewind or PlayBackwards/SmoothRewind, OnRewind callbacks will be fired only if the tween isn't already rewinded" : ((_src.rewindCallbackMode == RewindCallbackMode.FireAlwaysWithRewind) ? "When calling Rewind, OnRewind callbacks will always be fired, even if the tween is already rewinded." : "When calling Rewind or PlayBackwards/SmoothRewind, OnRewind callbacks will always be fired, even if the tween is already rewinded"), MessageType.None);
			GUILayout.EndHorizontal();
			_src.showUnityEditorReport = EditorGUILayout.Toggle("Editor Report", _src.showUnityEditorReport);
			_src.logBehaviour = (LogBehaviour)(object)EditorGUILayout.EnumPopup("Log Behaviour", _src.logBehaviour);
			_src.drawGizmos = EditorGUILayout.Toggle("Draw Path Gizmos", _src.drawGizmos);
			DOTweenSettings.SettingsLocation storeSettingsLocation = _src.storeSettingsLocation;
			_src.storeSettingsLocation = (DOTweenSettings.SettingsLocation)EditorGUILayout.Popup("Settings Location", (int)_src.storeSettingsLocation, _settingsLocation);
			if (_src.storeSettingsLocation != storeSettingsLocation)
			{
				if (_src.storeSettingsLocation == DOTweenSettings.SettingsLocation.DemigiantDirectory && EditorUtils.demigiantDir == null)
				{
					EditorUtility.DisplayDialog("Change DOTween Settings Location", "Demigiant directory not present (must be the parent of DOTween's directory)", "Ok");
					if (storeSettingsLocation == DOTweenSettings.SettingsLocation.DemigiantDirectory)
					{
						_src.storeSettingsLocation = DOTweenSettings.SettingsLocation.AssetsDirectory;
						Connect(true);
					}
					else
					{
						_src.storeSettingsLocation = storeSettingsLocation;
					}
				}
				else
				{
					Connect(true);
				}
			}
			GUILayout.Space(8f);
			GUILayout.Label("DEFAULTS ▼");
			_src.defaultRecyclable = EditorGUILayout.Toggle("Recycle Tweens", _src.defaultRecyclable);
			_src.defaultAutoPlay = (AutoPlay)(object)EditorGUILayout.EnumPopup("AutoPlay", _src.defaultAutoPlay);
			_src.defaultUpdateType = (UpdateType)(object)EditorGUILayout.EnumPopup("Update Type", _src.defaultUpdateType);
			_src.defaultTimeScaleIndependent = EditorGUILayout.Toggle("TimeScale Independent", _src.defaultTimeScaleIndependent);
			_src.defaultEaseType = (Ease)(object)EditorGUILayout.EnumPopup("Ease", _src.defaultEaseType);
			_src.defaultEaseOvershootOrAmplitude = EditorGUILayout.FloatField("Ease Overshoot", _src.defaultEaseOvershootOrAmplitude);
			_src.defaultEasePeriod = EditorGUILayout.FloatField("Ease Period", _src.defaultEasePeriod);
			_src.defaultAutoKill = EditorGUILayout.Toggle("AutoKill", _src.defaultAutoKill);
			_src.defaultLoopType = (LoopType)(object)EditorGUILayout.EnumPopup("Loop Type", _src.defaultLoopType);
			GUILayout.Space(8f);
			_src.debugMode = EditorGUIUtils.ToggleButton(_src.debugMode, new GUIContent("DEBUG MODE", "Turns debug mode options on/off"), true, null);
			if (_src.debugMode)
			{
				GUILayout.BeginVertical(GUI.skin.box);
				EditorGUI.BeginDisabledGroup(!_src.useSafeMode && _src.logBehaviour != LogBehaviour.ErrorsOnly);
				_src.debugStoreTargetId = EditorGUILayout.Toggle("Store GameObject's ID", _src.debugStoreTargetId);
				GUILayout.Label("<b>Requires Safe Mode to be active + Default or Verbose LogBehaviour:</b> when using DO shortcuts stores the relative gameObject's name so it can be returned along the warning logs (helps with a clearer identification of the warning's target)", EditorGUIUtils.wordWrapRichTextLabelStyle);
				EditorGUI.EndDisabledGroup();
				GUILayout.EndVertical();
			}
		}

		private void DrawUtilsGUI()
		{
			GUILayout.Space(40f);
			if (EditorUtils.hasPro)
			{
				using (new GUILayout.VerticalScope(GUI.skin.box))
				{
					GUILayout.Label("Fix <b>DOTweenPro</b> MissingScript References", EditorGUIUtils.wordWrapRichTextLabelStyle);
					if (GUILayout.Button("In <b>current</b> Scene", EditorGUIUtils.btSetup) && EditorUtility.DisplayDialog("Fix MissingScript References", "Inspect current scene for missing DOTweenPro script References and fix them?\n\n(MAKE A BACKUP first!)", "Ok", "Cancel"))
					{
						Dispatch_OnRequestDOTweenProMissingScriptsFix(true);
					}
					if (GUILayout.Button("In <b>all</b> Scenes and Prefabs", EditorGUIUtils.btSetup) && EditorUtility.DisplayDialog("Fix MissingScript References", "Inspect all your project's Prefabs and Scenes to find missing DOTweenPro script References and fix them?\n\n(MAKE A BACKUP first!)", "Ok", "Cancel"))
					{
						Dispatch_OnRequestDOTweenProMissingScriptsFix(false);
					}
					GUILayout.Space(4f);
				}
			}
			if (!EditorUtils.hasDOTweenTimeline)
			{
				return;
			}
			using (new GUILayout.VerticalScope(GUI.skin.box))
			{
				GUILayout.Label("Fix <b>DOTweenTimeline</b> MissingScript References", EditorGUIUtils.wordWrapRichTextLabelStyle);
				if (GUILayout.Button("In <b>current</b> Scene", EditorGUIUtils.btSetup) && EditorUtility.DisplayDialog("Fix MissingScript References", "Inspect current scene for missing DOTweenTimeline script References and fix them?\n\n(MAKE A BACKUP first!)", "Ok", "Cancel"))
				{
					Dispatch_OnRequestDOTweenTimelineMissingScriptsFix(true);
				}
				if (GUILayout.Button("In <b>all</b> Scenes and Prefabs", EditorGUIUtils.btSetup) && EditorUtility.DisplayDialog("Fix MissingScript References", "Inspect all your project's Prefabs and Scenes to find missing DOTweenTimeline script References and fix them?\n\n(MAKE A BACKUP first!)", "Ok", "Cancel"))
				{
					Dispatch_OnRequestDOTweenTimelineMissingScriptsFix(false);
				}
				GUILayout.Space(4f);
			}
		}

		public static DOTweenSettings GetDOTweenSettings()
		{
			return ConnectToSource(null, false, false);
		}

		private static DOTweenSettings ConnectToSource(DOTweenSettings src, bool createIfMissing, bool fullSetup)
		{
			LocationData locationData = new LocationData(EditorUtils.assetsPath + EditorUtils.pathSlash + "Resources");
			LocationData locationData2 = new LocationData(EditorUtils.dotweenDir + "Resources");
			bool flag = EditorUtils.demigiantDir != null;
			LocationData locationData3 = (flag ? new LocationData(EditorUtils.demigiantDir + "Resources") : default(LocationData));
			if (src == null)
			{
				src = EditorUtils.ConnectToSourceAsset<DOTweenSettings>(locationData.adbFilePath);
				if (src == null)
				{
					src = EditorUtils.ConnectToSourceAsset<DOTweenSettings>(locationData2.adbFilePath);
				}
				if (src == null && flag)
				{
					src = EditorUtils.ConnectToSourceAsset<DOTweenSettings>(locationData3.adbFilePath);
				}
			}
			if (src == null)
			{
				if (!createIfMissing)
				{
					return null;
				}
				if (!Directory.Exists(locationData.dir))
				{
					AssetDatabase.CreateFolder(locationData.adbParentDir, "Resources");
				}
				src = EditorUtils.ConnectToSourceAsset<DOTweenSettings>(locationData.adbFilePath, true);
			}
			if (fullSetup)
			{
				switch (src.storeSettingsLocation)
				{
				case DOTweenSettings.SettingsLocation.AssetsDirectory:
					src = MoveSrc(new LocationData[2] { locationData2, locationData3 }, locationData);
					break;
				case DOTweenSettings.SettingsLocation.DOTweenDirectory:
					src = MoveSrc(new LocationData[2] { locationData, locationData3 }, locationData2);
					break;
				case DOTweenSettings.SettingsLocation.DemigiantDirectory:
					src = MoveSrc(new LocationData[2] { locationData, locationData2 }, locationData3);
					break;
				}
			}
			return src;
		}

		private void Connect(bool forceReconnect = false)
		{
			if (!(_src != null) || forceReconnect)
			{
				_src = ConnectToSource(_src, true, true);
			}
		}

		private static DOTweenSettings MoveSrc(LocationData[] from, LocationData to)
		{
			if (!Directory.Exists(to.dir))
			{
				AssetDatabase.CreateFolder(to.adbParentDir, "Resources");
			}
			for (int i = 0; i < from.Length; i++)
			{
				LocationData locationData = from[i];
				if (File.Exists(locationData.filePath))
				{
					AssetDatabase.MoveAsset(locationData.adbFilePath, to.adbFilePath);
					AssetDatabase.DeleteAsset(locationData.adbFilePath);
					if (Directory.GetDirectories(locationData.dir).Length == 0 && Directory.GetFiles(locationData.dir).Length == 0)
					{
						AssetDatabase.DeleteAsset(EditorUtils.FullPathToADBPath(locationData.dir));
					}
				}
			}
			return EditorUtils.ConnectToSourceAsset<DOTweenSettings>(to.adbFilePath, true);
		}
	}
}
