using System.IO;
using DG.DOTweenEditor.UI;
using DG.Tweening.Core;
using UnityEditor;
using UnityEngine;

namespace DG.DOTweenEditor
{
	internal static class ASMDEFManager
	{
		public enum ASMDEFType
		{
			Modules,
			DOTweenPro,
			DOTweenProEditor,
			DOTweenTimeline,
			DOTweenTimelineEditor
		}

		private enum ChangeType
		{
			Deleted,
			Created,
			Overwritten
		}

		private const string _ModulesId = "DOTween.Modules";

		private const string _ProId = "DOTweenPro.Scripts";

		private const string _ProEditorId = "DOTweenPro.EditorScripts";

		private const string _DOTweenTimelineId = "DOTweenTimeline.Scripts";

		private const string _DOTweenTimelineEditorId = "DOTweenTimeline.EditorScripts";

		private const string _ModulesASMDEFFile = "DOTween.Modules.asmdef";

		private const string _ProASMDEFFile = "DOTweenPro.Scripts.asmdef";

		private const string _ProEditorASMDEFFile = "DOTweenPro.EditorScripts.asmdef";

		private const string _DOTweenTimelineASMDEFFile = "DOTweenTimeline.Scripts.asmdef";

		private const string _DOTweenTimelineEditorASMDEFFile = "DOTweenTimeline.EditorScripts.asmdef";

		private const string _RefTextMeshPro = "Unity.TextMeshPro";

		public static bool hasModulesASMDEF { get; private set; }

		public static bool hasProASMDEF { get; private set; }

		public static bool hasProEditorASMDEF { get; private set; }

		public static bool hasDOTweenTimelineASMDEF { get; private set; }

		public static bool hasDOTweenTimelineEditorASMDEF { get; private set; }

		static ASMDEFManager()
		{
			Refresh();
		}

		public static void ApplyASMDEFSettings()
		{
			Refresh();
			DOTweenSettings dOTweenSettings = DOTweenUtilityWindow.GetDOTweenSettings();
			if (dOTweenSettings != null)
			{
				if (dOTweenSettings.createASMDEF)
				{
					CreateAllASMDEF();
				}
				else
				{
					RemoveAllASMDEF();
				}
			}
			RefreshExistingASMDEFFiles();
		}

		public static void Refresh()
		{
			hasModulesASMDEF = File.Exists(EditorUtils.dotweenModulesDir + "DOTween.Modules.asmdef");
			hasProASMDEF = File.Exists(EditorUtils.dotweenProDir + "DOTweenPro.Scripts.asmdef");
			hasProEditorASMDEF = File.Exists(EditorUtils.dotweenProEditorDir + "DOTweenPro.EditorScripts.asmdef");
			hasDOTweenTimelineASMDEF = File.Exists(EditorUtils.dotweenTimelineScriptsDir + "DOTweenTimeline.Scripts.asmdef");
			hasDOTweenTimelineEditorASMDEF = File.Exists(EditorUtils.dotweenTimelineEditorScriptsDir + "DOTweenTimeline.EditorScripts.asmdef");
		}

		public static void RefreshExistingASMDEFFiles()
		{
			Refresh();
			DOTweenSettings dOTweenSettings = DOTweenUtilityWindow.GetDOTweenSettings();
			if (!(dOTweenSettings == null) && dOTweenSettings.createASMDEF)
			{
				if (EditorUtils.hasPro)
				{
					ValidateProASMDEFReferences(dOTweenSettings, ASMDEFType.DOTweenPro, EditorUtils.dotweenProDir + "DOTweenPro.Scripts.asmdef");
					ValidateProASMDEFReferences(dOTweenSettings, ASMDEFType.DOTweenProEditor, EditorUtils.dotweenProEditorDir + "DOTweenPro.EditorScripts.asmdef");
				}
				if (EditorUtils.hasDOTweenTimeline)
				{
					ValidateDOTweenTimelineASMDEFReferences(dOTweenSettings, ASMDEFType.DOTweenTimeline, EditorUtils.dotweenTimelineScriptsDir + "DOTweenTimeline.Scripts.asmdef");
					ValidateDOTweenTimelineASMDEFReferences(dOTweenSettings, ASMDEFType.DOTweenTimelineEditor, EditorUtils.dotweenTimelineEditorScriptsDir + "DOTweenTimeline.EditorScripts.asmdef");
				}
			}
		}

		public static void CreateAllASMDEF()
		{
			DOTweenSettings dOTweenSettings = DOTweenUtilityWindow.GetDOTweenSettings();
			dOTweenSettings.createASMDEF = true;
			EditorUtility.SetDirty(dOTweenSettings);
			if (!hasModulesASMDEF)
			{
				CreateASMDEF(ASMDEFType.Modules);
			}
			if (EditorUtils.hasPro)
			{
				if (!hasProASMDEF)
				{
					CreateASMDEF(ASMDEFType.DOTweenPro);
				}
				if (!hasProEditorASMDEF)
				{
					CreateASMDEF(ASMDEFType.DOTweenProEditor);
				}
			}
			if (EditorUtils.hasDOTweenTimeline)
			{
				if (!hasDOTweenTimelineASMDEF)
				{
					CreateASMDEF(ASMDEFType.DOTweenTimeline);
				}
				if (!hasDOTweenTimelineEditorASMDEF)
				{
					CreateASMDEF(ASMDEFType.DOTweenTimelineEditor);
				}
			}
		}

		public static void RemoveAllASMDEF()
		{
			DOTweenSettings dOTweenSettings = DOTweenUtilityWindow.GetDOTweenSettings();
			dOTweenSettings.createASMDEF = false;
			EditorUtility.SetDirty(dOTweenSettings);
			RemoveASMDEF(ASMDEFType.Modules);
			if (hasProASMDEF)
			{
				RemoveASMDEF(ASMDEFType.DOTweenPro);
			}
			if (hasProEditorASMDEF)
			{
				RemoveASMDEF(ASMDEFType.DOTweenProEditor);
			}
			if (hasDOTweenTimelineASMDEF)
			{
				RemoveASMDEF(ASMDEFType.DOTweenTimeline);
			}
			if (hasDOTweenTimelineEditorASMDEF)
			{
				RemoveASMDEF(ASMDEFType.DOTweenTimelineEditor);
			}
		}

		private static void ValidateProASMDEFReferences(DOTweenSettings src, ASMDEFType asmdefType, string asmdefFilepath)
		{
			bool flag = false;
			using (StreamReader streamReader = new StreamReader(asmdefFilepath))
			{
				string text;
				while ((text = streamReader.ReadLine()) != null)
				{
					if (text.Contains("Unity.TextMeshPro"))
					{
						flag = true;
						break;
					}
				}
			}
			if (flag != src.modules.textMeshProEnabled)
			{
				CreateASMDEF(asmdefType, true);
			}
		}

		private static void ValidateDOTweenTimelineASMDEFReferences(DOTweenSettings src, ASMDEFType asmdefType, string asmdefFilepath)
		{
			bool flag = false;
			using (StreamReader streamReader = new StreamReader(asmdefFilepath))
			{
				string text;
				while ((text = streamReader.ReadLine()) != null)
				{
					if (text.Contains("Unity.TextMeshPro"))
					{
						flag = true;
						break;
					}
				}
			}
			if (flag != src.modules.textMeshProEnabled)
			{
				CreateASMDEF(asmdefType, true);
			}
		}

		private static void LogASMDEFChange(ASMDEFType asmdefType, ChangeType changeType)
		{
			string arg = "";
			switch (asmdefType)
			{
			case ASMDEFType.Modules:
				arg = "DOTween/Modules/DOTween.Modules.asmdef";
				break;
			case ASMDEFType.DOTweenPro:
				arg = "DOTweenPro/DOTweenPro.Scripts.asmdef";
				break;
			case ASMDEFType.DOTweenProEditor:
				arg = "DOTweenPro/Editor/DOTweenPro.EditorScripts.asmdef";
				break;
			case ASMDEFType.DOTweenTimeline:
				arg = "DOTweenTimeline/Scripts/DOTweenTimeline.Scripts.asmdef";
				break;
			case ASMDEFType.DOTweenTimelineEditor:
				arg = "DOTweenTimeline/Scripts/Editor/DOTweenTimeline.EditorScripts.asmdef";
				break;
			}
			object arg2;
			switch (changeType)
			{
			default:
				arg2 = "ff6600";
				break;
			case ChangeType.Created:
				arg2 = "00ff00";
				break;
			case ChangeType.Deleted:
				arg2 = "ff0000";
				break;
			}
			object arg3;
			switch (changeType)
			{
			default:
				arg3 = "changed";
				break;
			case ChangeType.Created:
				arg3 = "created";
				break;
			case ChangeType.Deleted:
				arg3 = "removed";
				break;
			}
			Debug.Log($"<b>DOTween ASMDEF file <color=#{arg2}>{arg3}</color></b> ► {arg}");
		}

		private static void CreateASMDEF(ASMDEFType type, bool forceOverwrite = false)
		{
			Refresh();
			bool flag = false;
			string arg = null;
			string text = null;
			string text2 = null;
			switch (type)
			{
			case ASMDEFType.Modules:
				flag = hasModulesASMDEF;
				arg = "DOTween.Modules";
				text = "DOTween.Modules.asmdef";
				text2 = EditorUtils.dotweenModulesDir;
				break;
			case ASMDEFType.DOTweenPro:
				flag = hasProASMDEF;
				arg = "DOTweenPro.Scripts";
				text = "DOTweenPro.Scripts.asmdef";
				text2 = EditorUtils.dotweenProDir;
				break;
			case ASMDEFType.DOTweenProEditor:
				flag = hasProEditorASMDEF;
				arg = "DOTweenPro.EditorScripts";
				text = "DOTweenPro.EditorScripts.asmdef";
				text2 = EditorUtils.dotweenProEditorDir;
				break;
			case ASMDEFType.DOTweenTimeline:
				flag = hasDOTweenTimelineASMDEF;
				arg = "DOTweenTimeline.Scripts";
				text = "DOTweenTimeline.Scripts.asmdef";
				text2 = EditorUtils.dotweenTimelineScriptsDir;
				break;
			case ASMDEFType.DOTweenTimelineEditor:
				flag = hasDOTweenTimelineEditorASMDEF;
				arg = "DOTweenTimeline.EditorScripts";
				text = "DOTweenTimeline.EditorScripts.asmdef";
				text2 = EditorUtils.dotweenTimelineEditorScriptsDir;
				break;
			}
			if (flag && !forceOverwrite)
			{
				return;
			}
			if (!Directory.Exists(text2))
			{
				EditorUtility.DisplayDialog("Create ASMDEF", $"Directory not found\n({text2})", "Ok");
				return;
			}
			string text3 = text2 + text;
			using (StreamWriter streamWriter = File.CreateText(text3))
			{
				streamWriter.WriteLine("{");
				switch (type)
				{
				case ASMDEFType.Modules:
					streamWriter.WriteLine("\t\"name\": \"{0}\"", arg);
					break;
				case ASMDEFType.DOTweenPro:
				case ASMDEFType.DOTweenProEditor:
				{
					streamWriter.WriteLine("\t\"name\": \"{0}\",", arg);
					streamWriter.WriteLine("\t\"references\": [");
					DOTweenSettings dOTweenSettings = DOTweenUtilityWindow.GetDOTweenSettings();
					if (dOTweenSettings != null && dOTweenSettings.modules.textMeshProEnabled)
					{
						streamWriter.WriteLine("\t\t\"{0}\",", "Unity.TextMeshPro");
					}
					if (type == ASMDEFType.DOTweenProEditor)
					{
						streamWriter.WriteLine("\t\t\"{0}\",", "DOTween.Modules");
						streamWriter.WriteLine("\t\t\"{0}\"", "DOTweenPro.Scripts");
						streamWriter.WriteLine("\t],");
						streamWriter.WriteLine("\t\"includePlatforms\": [");
						streamWriter.WriteLine("\t\t\"Editor\"");
						streamWriter.WriteLine("\t],");
						streamWriter.WriteLine("\t\"autoReferenced\": false");
					}
					else
					{
						streamWriter.WriteLine("\t\t\"{0}\"", "DOTween.Modules");
						streamWriter.WriteLine("\t]");
					}
					break;
				}
				case ASMDEFType.DOTweenTimeline:
				case ASMDEFType.DOTweenTimelineEditor:
				{
					streamWriter.WriteLine("\t\"name\": \"{0}\",", arg);
					streamWriter.WriteLine("\t\"references\": [");
					DOTweenSettings dOTweenSettings = DOTweenUtilityWindow.GetDOTweenSettings();
					if (dOTweenSettings != null && dOTweenSettings.modules.textMeshProEnabled)
					{
						streamWriter.WriteLine("\t\t\"{0}\",", "Unity.TextMeshPro");
					}
					if (type == ASMDEFType.DOTweenTimelineEditor)
					{
						if (EditorUtils.hasPro)
						{
							streamWriter.WriteLine("\t\t\"{0}\",", "DOTweenPro.Scripts");
						}
						streamWriter.WriteLine("\t\t\"{0}\",", "DOTween.Modules");
						streamWriter.WriteLine("\t\t\"{0}\"", "DOTweenTimeline.Scripts");
						streamWriter.WriteLine("\t],");
						streamWriter.WriteLine("\t\"includePlatforms\": [");
						streamWriter.WriteLine("\t\t\"Editor\"");
						streamWriter.WriteLine("\t],");
						streamWriter.WriteLine("\t\"autoReferenced\": false");
					}
					else
					{
						if (EditorUtils.hasPro)
						{
							streamWriter.WriteLine("\t\t\"{0}\",", "DOTweenPro.Scripts");
						}
						streamWriter.WriteLine("\t\t\"{0}\"", "DOTween.Modules");
						streamWriter.WriteLine("\t]");
					}
					break;
				}
				}
				streamWriter.WriteLine("}");
			}
			AssetDatabase.ImportAsset(EditorUtils.FullPathToADBPath(text3), ImportAssetOptions.ForceUpdate);
			Refresh();
			LogASMDEFChange(type, (!flag) ? ChangeType.Created : ChangeType.Overwritten);
		}

		private static void RemoveASMDEF(ASMDEFType type)
		{
			bool flag = false;
			string text = null;
			string text2 = null;
			switch (type)
			{
			case ASMDEFType.Modules:
				flag = hasModulesASMDEF;
				text2 = EditorUtils.dotweenModulesDir;
				text = "DOTween.Modules.asmdef";
				break;
			case ASMDEFType.DOTweenPro:
				flag = hasProASMDEF;
				text = "DOTweenPro.Scripts.asmdef";
				text2 = EditorUtils.dotweenProDir;
				break;
			case ASMDEFType.DOTweenProEditor:
				flag = hasProEditorASMDEF;
				text = "DOTweenPro.EditorScripts.asmdef";
				text2 = EditorUtils.dotweenProEditorDir;
				break;
			case ASMDEFType.DOTweenTimeline:
				flag = hasDOTweenTimelineASMDEF;
				text = "DOTweenTimeline.Scripts.asmdef";
				text2 = EditorUtils.dotweenTimelineScriptsDir;
				break;
			case ASMDEFType.DOTweenTimelineEditor:
				flag = hasDOTweenTimelineEditorASMDEF;
				text = "DOTweenTimeline.EditorScripts.asmdef";
				text2 = EditorUtils.dotweenTimelineEditorScriptsDir;
				break;
			}
			Refresh();
			if (flag)
			{
				AssetDatabase.DeleteAsset(EditorUtils.FullPathToADBPath(text2 + text));
				Refresh();
				LogASMDEFChange(type, ChangeType.Deleted);
			}
		}
	}
}
