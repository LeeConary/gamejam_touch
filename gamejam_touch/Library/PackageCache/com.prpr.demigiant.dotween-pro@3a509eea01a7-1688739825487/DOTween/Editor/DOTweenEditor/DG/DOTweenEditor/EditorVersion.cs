using System.Globalization;
using UnityEngine;

namespace DG.DOTweenEditor
{
	internal static class EditorVersion
	{
		/// <summary>Full major version + first minor version (ex: 2018.1f)</summary>
		public static readonly float Version;

		/// <summary>Major version</summary>
		public static readonly int MajorVersion;

		/// <summary>First minor version (ex: in 2018.1 it would be 1)</summary>
		public static readonly int MinorVersion;

		static EditorVersion()
		{
			string unityVersion = Application.unityVersion;
			int num = unityVersion.IndexOf('.');
			if (num == -1)
			{
				MajorVersion = int.Parse(unityVersion);
				Version = MajorVersion;
				return;
			}
			string text = unityVersion.Substring(0, num);
			MajorVersion = int.Parse(text);
			string text2 = unityVersion.Substring(num + 1);
			num = text2.IndexOf('.');
			if (num != -1)
			{
				text2 = text2.Substring(0, num);
			}
			MinorVersion = int.Parse(text2);
			if (!float.TryParse(text + "." + text2, NumberStyles.Float, CultureInfo.InvariantCulture, out Version))
			{
				Debug.LogWarning($"DOTweenEditor.EditorVersion ► Error when detecting Unity Version from \"{text}.{text2}\"");
				Version = 2018.3f;
			}
		}
	}
}
