using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace DG.DemiEditor
{
	public static class DeEditorHierarchyUtils
	{
		private static MethodInfo _miExpand;

		public static void ExpandGameObject(GameObject go)
		{
			if (_miExpand == null)
			{
				Type type = Type.GetType("UnityEditor.SceneHierarchyWindow,UnityEditor.dll");
				if (type == null)
				{
					Debug.LogError("Couldn't find type \"UnityEditor.SceneHierarchyWindow,UnityEditor.dll\"");
					return;
				}
				_miExpand = type.GetMethod("ExpandTreeViewItem", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
				if (_miExpand == null)
				{
					_miExpand = type.GetMethod("SetExpandedRecursive", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
				}
				if (_miExpand == null)
				{
					Debug.LogError("Couldn't find SceneHierarchyWindow \"ExpandTreeViewItem\" or \"SetExpandedRecursive\" methods");
					return;
				}
			}
			if (DeUnityEditorVersion.MajorVersion < 2019)
			{
				EditorApplication.ExecuteMenuItem("Window/Hierarchy");
			}
			else
			{
				EditorApplication.ExecuteMenuItem("Window/General/Hierarchy");
			}
			EditorWindow focusedWindow = EditorWindow.focusedWindow;
			_miExpand.Invoke(focusedWindow, new object[2]
			{
				go.GetInstanceID(),
				true
			});
		}
	}
}
