using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DemiEditor")]
[assembly: AssemblyDescription("Utility for the skinning and implementation of various editor GUI elements")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Demigiant")]
[assembly: AssemblyProduct("DemiEditor")]
[assembly: AssemblyCopyright("Copyright © Daniele Giardini, Demigiant 2015")]
[assembly: AssemblyTrademark("")]
[assembly: InternalsVisibleTo("DeEditorTools")]
[assembly: ComVisible(false)]
[assembly: Guid("807e068c-2a0e-4c81-a303-4b4fd3924511")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
