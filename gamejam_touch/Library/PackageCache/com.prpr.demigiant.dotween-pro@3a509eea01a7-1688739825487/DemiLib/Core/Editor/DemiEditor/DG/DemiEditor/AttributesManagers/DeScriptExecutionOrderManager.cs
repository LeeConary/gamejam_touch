using System;
using DG.DemiLib.Attributes;
using UnityEditor;

namespace DG.DemiEditor.AttributesManagers
{
	[InitializeOnLoad]
	public class DeScriptExecutionOrderManager
	{
		static DeScriptExecutionOrderManager()
		{
			if (DeEditorUtils.isUnityReady)
			{
				Execute();
				return;
			}
			DeEditorNotification.OnUnityReady -= Execute;
			DeEditorNotification.OnUnityReady += Execute;
		}

		private static void Execute()
		{
			DeEditorNotification.OnUnityReady -= Execute;
			MonoScript[] allRuntimeMonoScripts = MonoImporter.GetAllRuntimeMonoScripts();
			int num = allRuntimeMonoScripts.Length;
			if (num <= 0)
			{
				return;
			}
			Type typeFromHandle = typeof(DeScriptExecutionOrderAttribute);
			for (int i = 0; i < num; i++)
			{
				MonoScript monoScript = allRuntimeMonoScripts[i];
				Type @class = monoScript.GetClass();
				if (@class == null || !@class.IsDefined(typeFromHandle, true))
				{
					continue;
				}
				object[] customAttributes = @class.GetCustomAttributes(typeFromHandle, true);
				int num2 = customAttributes.Length;
				for (int j = 0; j < num2; j++)
				{
					Attribute attribute = (Attribute)customAttributes[j];
					int executionOrder = MonoImporter.GetExecutionOrder(monoScript);
					int order = ((DeScriptExecutionOrderAttribute)attribute).order;
					if (executionOrder != order)
					{
						MonoImporter.SetExecutionOrder(monoScript, order);
					}
				}
			}
		}
	}
}
