namespace DG.DemiEditor
{
	/// <summary>
	/// Extend any custom subpalettes from this, so they will be initialized correctly
	/// </summary>
	public abstract class DeStyleSubPalette
	{
		public abstract void Init();
	}
}
