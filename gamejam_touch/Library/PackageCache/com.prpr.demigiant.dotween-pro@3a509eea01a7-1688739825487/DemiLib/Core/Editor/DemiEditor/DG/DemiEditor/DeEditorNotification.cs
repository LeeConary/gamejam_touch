using System;
using UnityEditor;

namespace DG.DemiEditor
{
	[InitializeOnLoad]
	public static class DeEditorNotification
	{
		/// <summary>
		/// Dispatched when Unity has finished compiling code and updating the AssetDatabase
		/// </summary>
		public static event Action OnUnityReady;

		private static void Dispatch_OnUnityReady()
		{
			if (DeEditorNotification.OnUnityReady != null)
			{
				DeEditorNotification.OnUnityReady();
			}
		}

		static DeEditorNotification()
		{
			if (DeEditorUtils.isUnityReady)
			{
				Dispatch_OnUnityReady();
			}
			else
			{
				EditorApplication.update = (EditorApplication.CallbackFunction)Delegate.Combine(EditorApplication.update, new EditorApplication.CallbackFunction(OnUnityReadyChecker));
			}
		}

		private static void OnUnityReadyChecker()
		{
			if (DeEditorUtils.isUnityReady)
			{
				EditorApplication.update = (EditorApplication.CallbackFunction)Delegate.Remove(EditorApplication.update, new EditorApplication.CallbackFunction(OnUnityReadyChecker));
				Dispatch_OnUnityReady();
			}
		}
	}
}
