using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DG.DemiEditor
{
	/// <summary>
	/// Framework used to fix missing monoScript reference in GameObjects when a script's meta guid changes
	/// </summary>
	public static class DeEditorMetaFixer
	{
		public class ComponentData
		{
			public readonly string id;

			public readonly string correctGuid;

			public readonly string[] serializedIdentifiers;

			public int totGuidsFixed;

			public ComponentData(string id, string correctGuid, params string[] serializedIdentifiers)
			{
				this.id = id;
				this.correctGuid = correctGuid;
				this.serializedIdentifiers = serializedIdentifiers;
			}
		}

		private class ModInfo
		{
			private readonly Dictionary<string, List<string>> _sceneOrPrefabADBFileToGosNames = new Dictionary<string, List<string>>();

			public void AddModifiedGameObjectName(string sceneOrPrefabADBFile, string goName)
			{
				if (!_sceneOrPrefabADBFileToGosNames.ContainsKey(sceneOrPrefabADBFile))
				{
					_sceneOrPrefabADBFileToGosNames.Add(sceneOrPrefabADBFile, new List<string>());
				}
				if (!_sceneOrPrefabADBFileToGosNames[sceneOrPrefabADBFile].Contains(goName))
				{
					_sceneOrPrefabADBFileToGosNames[sceneOrPrefabADBFile].Add(goName);
				}
			}

			public List<string> GetModifiedGameObjectNamesFor(string sceneOrPrefabADBFile)
			{
				if (!_sceneOrPrefabADBFileToGosNames.ContainsKey(sceneOrPrefabADBFile))
				{
					return null;
				}
				return _sceneOrPrefabADBFileToGosNames[sceneOrPrefabADBFile];
			}

			public void AddDetailedModInfoTo(StringBuilder addToStrb, string adbModifiedFile)
			{
				List<string> modifiedGameObjectNamesFor = GetModifiedGameObjectNamesFor(adbModifiedFile);
				if (modifiedGameObjectNamesFor == null)
				{
					return;
				}
				addToStrb.Append("\n- ").Append(adbModifiedFile.Substring(8));
				addToStrb.Append(" ► GOs modified: ");
				for (int i = 0; i < modifiedGameObjectNamesFor.Count; i++)
				{
					if (i > 0)
					{
						addToStrb.Append(", ");
					}
					string value = modifiedGameObjectNamesFor[i];
					addToStrb.Append('"').Append(value).Append('"');
				}
			}
		}

		private static string _currSceneADBFilePath;

		private static IEnumerator _opsCoroutine;

		/// <summary>
		/// Retrieves the GUID in the given meta file and returns it, or NULL if it's not found
		/// </summary>
		/// <param name="metaFilePath">Full filePath to the meta file</param>
		public static string RetrieveMetaGuid(string metaFilePath)
		{
			if (!File.Exists(metaFilePath))
			{
				Debug.LogWarning($"DeEditorMetaFixer.RetrieveMetaGuid ► meta file doesn't exist ({metaFilePath})");
				return null;
			}
			using (StreamReader streamReader = new StreamReader(metaFilePath))
			{
				string text;
				while ((text = streamReader.ReadLine()) != null)
				{
					if (text.StartsWith("guid:"))
					{
						return text.Substring(text.IndexOf(':') + 1).Trim();
					}
				}
			}
			Debug.LogWarning($"DeEditorMetaFixer.RetrieveMetaGuid ► GUID not found while reading \"{metaFilePath}\"");
			return null;
		}

		/// <summary>
		/// Fixes all wrong Component GUIDs in scenes and prefabs
		/// </summary>
		/// <param name="cDatas"><see cref="T:DG.DemiEditor.DeEditorMetaFixer.ComponentData" /> objects to use for the operation</param>
		public static void FixComponentsGuidsInAllScenesAndPrefabs(params ComponentData[] cDatas)
		{
			if (_opsCoroutine != null)
			{
				Debug.LogWarning("DeEditorMetaFixer.FixComponentsGuidsInAllScenesAndPrefabs ► Ignored because another operation is already running");
			}
			else
			{
				_opsCoroutine = DeEditorCoroutines.StartCoroutine(CO_FixComponentsGuidsInAllScenesAndPrefabs(cDatas));
			}
		}

		private static IEnumerator CO_FixComponentsGuidsInAllScenesAndPrefabs(ComponentData[] cDatas)
		{
			if (!BeginFixScenesOperation())
			{
				ClearOps();
				yield break;
			}
			EditorUtility.DisplayProgressBar("Fix MissingScripts", "Fixing MissingScript errors in all scenes and prefabs", 0f);
			yield return null;
			ModInfo modInfo = new ModInfo();
			int totGuidsFixed = 0;
			string[] array = (from f in Directory.GetFiles(DeEditorFileUtils.assetsPath, "*.*", SearchOption.AllDirectories)
				where f.EndsWith(".unity") || f.EndsWith(".prefab")
				select f).ToArray();
			int num = array.Length;
			List<string> list = new List<string>();
			int totModifiedScenes = 0;
			int totModifiedPrefabs = 0;
			AssetDatabase.StartAssetEditing();
			ComponentData[] array2;
			for (int i = 0; i < num; i++)
			{
				float progress = (float)(i + 1) / (float)num;
				string text = array[i];
				EditorUtility.DisplayProgressBar("Fix MissingScripts", "Checking/fixing MissingScript errors in " + Path.GetFileName(text), progress);
				try
				{
					string sceneOrPrefabFileString = File.ReadAllText(text);
					bool flag = false;
					array2 = cDatas;
					for (int j = 0; j < array2.Length; j++)
					{
						int num2 = FixComponentGuidInSceneOrPrefabString(array2[j], ref sceneOrPrefabFileString, DeEditorFileUtils.FullPathToADBPath(text), modInfo);
						totGuidsFixed += num2;
						if (num2 > 0)
						{
							flag = true;
						}
					}
					if (flag)
					{
						EditorUtility.DisplayProgressBar("Fix MissingScripts", "Write file " + Path.GetFileName(text), progress);
						File.WriteAllText(text, sceneOrPrefabFileString);
						list.Add(text);
						if (text.EndsWith(".unity"))
						{
							totModifiedScenes++;
						}
						else
						{
							totModifiedPrefabs++;
						}
					}
				}
				catch (Exception exception)
				{
					Debug.LogException(exception);
				}
			}
			AssetDatabase.StopAssetEditing();
			int count = list.Count;
			StringBuilder strb = new StringBuilder().Append(totGuidsFixed).Append(" GUIDs fixed in ").Append(totModifiedScenes)
				.Append(" scenes and ")
				.Append(totModifiedPrefabs)
				.Append(" prefabs");
			if (count > 0)
			{
				for (int k = 0; k < count; k++)
				{
					string text2 = DeEditorFileUtils.FullPathToADBPath(list[k]);
					AssetDatabase.ImportAsset(text2, ImportAssetOptions.ForceUpdate);
					modInfo.AddDetailedModInfoTo(strb, text2);
				}
			}
			EditorUtility.ClearProgressBar();
			Debug.Log(strb.ToString());
			yield return null;
			strb.Length = 0;
			strb.Append(totGuidsFixed).Append(" GUIDs fixed in ").Append(totModifiedScenes)
				.Append(" scenes and ")
				.Append(totModifiedPrefabs)
				.Append(" prefabs.");
			array2 = cDatas;
			foreach (ComponentData componentData in array2)
			{
				strb.Append("\n- ").Append(componentData.id).Append(": ")
					.Append(componentData.totGuidsFixed)
					.Append(" GUIDs fixed");
			}
			EditorUtility.DisplayDialog("Fix MissingScripts", strb.ToString(), "Ok");
			EndFixScenesOperation();
		}

		/// <summary>
		/// Fixes all wrong Component GUIDs in the active scene and returns the total number of Components fixed
		/// </summary>
		/// <param name="cDatas"><see cref="T:DG.DemiEditor.DeEditorMetaFixer.ComponentData" /> objects to use for the operation</param>
		public static void FixComponentsGuidsInActiveScene(params ComponentData[] cDatas)
		{
			if (_opsCoroutine != null)
			{
				Debug.LogWarning("DeEditorMetaFixer.FixComponentsGuidsInActiveScene ► Ignored because another operation is already running");
			}
			else
			{
				_opsCoroutine = DeEditorCoroutines.StartCoroutine(CO_FixComponentsGuidsInActiveScene(cDatas));
			}
		}

		private static IEnumerator CO_FixComponentsGuidsInActiveScene(ComponentData[] cDatas)
		{
			if (!BeginFixScenesOperation())
			{
				yield break;
			}
			EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
			EditorUtility.DisplayProgressBar("Fix MissingScripts", "Fixing MissingScript errors in active scene", 0f);
			yield return null;
			ModInfo modInfo = new ModInfo();
			string currSceneFullPath = DeEditorFileUtils.ADBPathToFullPath(_currSceneADBFilePath);
			int totCDatas = cDatas.Length;
			int totGuidsFixed = 0;
			string sceneFileString = File.ReadAllText(currSceneFullPath);
			for (int i = 0; i < totCDatas; i++)
			{
				ComponentData componentData = cDatas[i];
				EditorUtility.DisplayProgressBar("Fix MissingScripts", "Checking/fixing MissingScript errors for " + componentData.id, (float)i / (float)totCDatas);
				totGuidsFixed += FixComponentGuidInSceneOrPrefabString(componentData, ref sceneFileString, _currSceneADBFilePath, modInfo);
				yield return null;
			}
			if (totGuidsFixed > 0)
			{
				EditorUtility.DisplayProgressBar("Fix MissingScripts", "Completed: saving scene", 1f);
				AssetDatabase.StartAssetEditing();
				try
				{
					File.WriteAllText(currSceneFullPath, sceneFileString);
				}
				catch (Exception exception)
				{
					Debug.LogException(exception);
				}
				AssetDatabase.StopAssetEditing();
				AssetDatabase.ImportAsset(_currSceneADBFilePath, ImportAssetOptions.ForceUpdate);
				yield return null;
			}
			EditorUtility.ClearProgressBar();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(totGuidsFixed + " GUIDs fixed in active scene");
			modInfo.AddDetailedModInfoTo(stringBuilder, _currSceneADBFilePath);
			Debug.Log(stringBuilder.ToString());
			stringBuilder.Length = 0;
			foreach (ComponentData componentData2 in cDatas)
			{
				stringBuilder.Append("\n- ").Append(componentData2.id).Append(": ")
					.Append(componentData2.totGuidsFixed)
					.Append(" GUIDs fixed");
			}
			EditorUtility.DisplayDialog("Fix MissingScripts", totGuidsFixed + " GUIDs fixed in active scene.\n" + stringBuilder.ToString(), "Ok");
			EndFixScenesOperation();
		}

		private static bool BeginFixScenesOperation()
		{
			_currSceneADBFilePath = SceneManager.GetActiveScene().path;
			if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
			{
				ClearOps();
			}
			return true;
		}

		private static void EndFixScenesOperation()
		{
			ClearOps();
			EditorSceneManager.OpenScene(_currSceneADBFilePath, OpenSceneMode.Single);
		}

		private static void ClearOps()
		{
			EditorUtility.ClearProgressBar();
			_opsCoroutine = null;
		}

		/// <summary>
		/// Finds all MonoBehaviour/Behaviour/Component in the given scene/prefab file string
		/// that contain the given <see cref="F:DG.DemiEditor.DeEditorMetaFixer.ComponentData.serializedIdentifiers" />
		/// and replaces their GUID with the one passed (if different).<para />
		/// Returns the total number of Component GUIDs that were fixed
		/// </summary>
		private static int FixComponentGuidInSceneOrPrefabString(ComponentData cData, ref string sceneOrPrefabFileString, string sceneOrPrefabADBFile, ModInfo modInfo)
		{
			Queue<int> queue = null;
			int num = cData.serializedIdentifiers.Length;
			string text = null;
			int num2 = 0;
			using (StringReader stringReader = new StringReader(sceneOrPrefabFileString))
			{
				int num3 = -1;
				bool flag = false;
				bool flag2 = false;
				string goName = null;
				string text2 = null;
				int item = -1;
				int num4 = 0;
				string text3;
				while ((text3 = stringReader.ReadLine()) != null)
				{
					num3++;
					switch (text3)
					{
					case "GameObject:":
						flag = true;
						break;
					case "MonoBehaviour:":
					case "Behaviour:":
					case "Component:":
						flag2 = true;
						break;
					default:
						if (text3.StartsWith("-"))
						{
							flag = (flag2 = false);
							text2 = null;
							item = -1;
							num4 = 0;
						}
						break;
					}
					if (flag)
					{
						string text4 = text3.TrimStart(' ');
						if (text4.StartsWith("m_Name: "))
						{
							goName = text4.Substring(8);
							flag = false;
						}
					}
					else
					{
						if (!flag2)
						{
							continue;
						}
						string text5 = text3.TrimStart(' ');
						if (text5.StartsWith("m_Script: "))
						{
							text2 = text3;
							item = num3;
							continue;
						}
						int num5 = text5.IndexOf(':');
						if (num5 == -1)
						{
							continue;
						}
						string value = text5.Substring(0, num5);
						if (Array.IndexOf(cData.serializedIdentifiers, value) != -1)
						{
							num4++;
						}
						if (num4 != num)
						{
							continue;
						}
						int num6 = text2.IndexOf("guid: ") + 6;
						int num7 = text2.IndexOf(',', num6) - 1;
						string text6 = text2.Substring(num6, num7 - num6 + 1);
						if (text6 != cData.correctGuid && string.IsNullOrEmpty(AssetDatabase.GUIDToAssetPath(text6)))
						{
							modInfo.AddModifiedGameObjectName(sceneOrPrefabADBFile, goName);
							text = text6;
							if (queue == null)
							{
								queue = new Queue<int>();
							}
							queue.Enqueue(item);
							num2++;
						}
						flag2 = false;
						text2 = null;
						item = -1;
						num4 = 0;
					}
				}
			}
			cData.totGuidsFixed += num2;
			if (text == null)
			{
				return 0;
			}
			using (StringReader stringReader2 = new StringReader(sceneOrPrefabFileString))
			{
				using (StringWriter stringWriter = new StringWriter())
				{
					int num8 = -1;
					int num9 = queue.Dequeue();
					string text7;
					while ((text7 = stringReader2.ReadLine()) != null)
					{
						num8++;
						if (num8 == num9)
						{
							stringWriter.WriteLine(text7.Replace(text, cData.correctGuid));
							if (queue.Count > 0)
							{
								num9 = queue.Dequeue();
							}
						}
						else
						{
							stringWriter.WriteLine(text7);
						}
					}
					sceneOrPrefabFileString = stringWriter.ToString();
					return num2;
				}
			}
		}
	}
}
