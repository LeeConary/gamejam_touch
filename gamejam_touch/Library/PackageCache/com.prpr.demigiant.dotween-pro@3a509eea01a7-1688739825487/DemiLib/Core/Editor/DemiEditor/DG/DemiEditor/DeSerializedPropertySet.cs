using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace DG.DemiEditor
{
	/// <summary>
	/// Utility class. You can either use it as is via its constructor, which automatically retrieves all serializedProperties in the instance,
	/// or you can extend it so you can add as many public SerializedProperties as the SerializedProperties you want to access
	/// (their name must be the same as the serialized field they refer to)
	/// </summary>
	public class DeSerializedPropertySet
	{
		private readonly List<SerializedProperty> _props = new List<SerializedProperty>();

		/// <summary>
		/// Automatically retrieves all serializable properties on the given serializedObject,
		/// or only specific ones if propNames is specified
		/// </summary>
		public DeSerializedPropertySet(SerializedObject serializedObject, params string[] propNames)
		{
			if (propNames != null)
			{
				foreach (string text in propNames)
				{
					if (serializedObject.FindProperty(text) == null)
					{
						Debug.LogError("DeSerializedPropertySet ► No property named \"" + serializedObject.targetObject.GetType()?.ToString() + "." + text + "\" exists");
					}
					else
					{
						_props.Add(serializedObject.FindProperty(text));
					}
				}
			}
			else if (GetType() != typeof(DeSerializedPropertySet))
			{
				AutoFillFromChildClassFields(serializedObject);
			}
			else
			{
				AutoFillFromSerializedObjectFields(serializedObject);
			}
		}

		/// <summary>
		/// Draws all property fields. Remember to wrap this within <code>serializedObject.Update</code>
		/// and <code>serializedObject.ApplyModifiedProperties</code>
		/// </summary>
		public void DrawAllPropertyFields()
		{
			foreach (SerializedProperty prop in _props)
			{
				EditorGUILayout.PropertyField(prop);
			}
		}

		private void AutoFillFromSerializedObjectFields(SerializedObject serializedObject)
		{
			FieldInfo[] fields = serializedObject.targetObject.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			foreach (FieldInfo fieldInfo in fields)
			{
				if (((fieldInfo.IsPublic && !fieldInfo.IsDefined(typeof(NonSerializedAttribute), false)) || fieldInfo.IsDefined(typeof(SerializeField), false)) && serializedObject.FindProperty(fieldInfo.Name) != null)
				{
					_props.Add(serializedObject.FindProperty(fieldInfo.Name));
				}
			}
		}

		private void AutoFillFromChildClassFields(SerializedObject serializedObject)
		{
			FieldInfo[] fields = GetType().GetFields(BindingFlags.Instance | BindingFlags.Public);
			foreach (FieldInfo fieldInfo in fields)
			{
				if (fieldInfo.FieldType == typeof(SerializedProperty))
				{
					SerializedProperty serializedProperty = serializedObject.FindProperty(fieldInfo.Name);
					if (serializedProperty != null)
					{
						fieldInfo.SetValue(this, serializedProperty);
						_props.Add(serializedProperty);
					}
				}
			}
		}
	}
}
