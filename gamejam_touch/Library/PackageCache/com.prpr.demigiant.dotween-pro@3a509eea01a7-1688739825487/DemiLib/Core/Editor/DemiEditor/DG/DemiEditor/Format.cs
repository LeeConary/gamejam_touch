namespace DG.DemiEditor
{
	public enum Format
	{
		RichText,
		WordWrap,
		NoRichText,
		NoWordWrap
	}
}
