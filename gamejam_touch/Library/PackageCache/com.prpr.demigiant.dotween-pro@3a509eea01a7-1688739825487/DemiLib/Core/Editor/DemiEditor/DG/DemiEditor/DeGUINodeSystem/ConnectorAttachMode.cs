namespace DG.DemiEditor.DeGUINodeSystem
{
	public enum ConnectorAttachMode
	{
		AnySideCenter,
		BottomOrRightCenter,
		TopOrLeftCenter,
		AnyCorner,
		TopCorners,
		LeftCorners,
		RightCorners,
		BottomCorners,
		TopLeft,
		TopCenter,
		TopRight,
		MiddleLeft,
		MiddleCenter,
		MiddleRight,
		BottomLeft,
		BottomCenter,
		BottomRight
	}
}
