namespace DG.DemiEditor.DeGUINodeSystem
{
	public enum ConnectorMode
	{
		Straight,
		Curved,
		Smart
	}
}
