﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TouchElementType
{
    Normal,
    Multi,
    Movable,
}

public enum TouchElementState
{
    Default,
    Appearing,
    PerfectOn,
    Pressed,
    Disappearing,
}

public class KeyInputListener : MonoBehaviour
{
    private ElementHandler handler;
    public Dictionary<TouchElementType, List<KeyCode>> reflectionDic = new Dictionary<TouchElementType, List<KeyCode>>
    {
        {
            TouchElementType.Normal , new List<KeyCode>
            {
                KeyCode.W,
                KeyCode.A,
                KeyCode.S,
                KeyCode.D,
            }
        },
        {
            TouchElementType.Multi , new List<KeyCode>
            {
                KeyCode.J,
                KeyCode.K,
                KeyCode.L,
                KeyCode.I,
            }
        },
        {
            TouchElementType.Movable , new List<KeyCode>
            {
                KeyCode.Space,
            }
        },
    };

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (handler == null)
        {
            handler = GetComponent<ElementHandler>();
        }

        if (handler.gameState == GameState.Wait)
        {
            if (Input.anyKeyDown)
            {
                handler.SetGameState(GameState.Start);
            }
        }

        if (handler.gameState == GameState.Gaming)
        {
            foreach (var kvp in reflectionDic)
            {
                var listeningKeyCode = kvp.Value;
                for (int i = 0; i < listeningKeyCode.Count; i++)
                {
                    KeyCode curKey = listeningKeyCode[i];
                    if (Input.GetKeyDown(curKey))
                    {
                        Debug.Log("Cur key: " + curKey + " -- Cut elementType: " + kvp.Key);
                        handler.HandlerInput(curKey, kvp.Key);
                    }
                }
            }
        }
    }
    
}
