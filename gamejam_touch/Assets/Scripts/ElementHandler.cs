// Copyright (c) PeroPeroGames Co., Ltd.
// Author: LeeConary
// Created On: 2023-07-07 22:25
// Description:

using System;
using System.Collections.Generic;
using System.Linq;
using Element;
using UnityEngine;

public enum GameState
{
    Wait,
    Start,
    Gaming,
    End,
}

public class ElementHandler : MonoBehaviour
{
    public BaseElement prototype;

    public KeyInputListener inputListener;

    public AudioManager audioManager;

    private Transform rootParent;

    private Transform poolParent;

    public float createDurationConst = 1f;
    
    public float createDuration = 1f;

    public float createRate = 0.1f;

    public int maxEndCount = 20;

    private int m_CreateRateConst = 10;

    private float m_CreateRateOffset = 1f;

    private Stack<BaseElement> pool;

    private float timeTick = 0f;

    private GameUIManager uiManager;
    public GameState gameState { get; private set; }

    public void SetGameState(GameState state)
    {
        gameState = state;
        if (gameState == GameState.Wait)
        {
            ResetGame();
            uiManager.SetGameWaiting();
        }
        else if(gameState == GameState.Start)
        {
            uiManager.SetGameStart();
        }
        else if (gameState == GameState.Gaming)
        {
            createDuration = 1f + createDurationConst;
        }
        else if (gameState == GameState.End)
        {
            uiManager.SetGameEnd();
        }
    }

    public void AddElement()
    {
        if (prototype == null)
        {
            return;
        }
        BaseElement element;
        if (pool.Count <= 0 )
        {
            element = Instantiate(prototype.gameObject, rootParent).GetComponent<BaseElement>();
        }
        else
        {
            element = pool.Pop();
        }
        element.transform.SetParent(rootParent);
        element.Reset(this);
        element.Show();
    }

    public void RemoveElement(BaseElement element)
    {
        element.transform.SetParent(poolParent);
        element.transform.localPosition = Vector3.zero;
        pool.Push(element);
    }

    public BaseElement GetElement(TouchElementType elementType = TouchElementType.Normal, bool isDequeue = true)
    {
        BaseElement[] elements = rootParent.GetComponentsInChildren<BaseElement>();
        if (elements.Length > 0)
        {
            return elements[0];
        }

        return null;
    }

    public void HandlerInput(KeyCode keyCode, TouchElementType elementType)
    {
        var element = GetElement(elementType, false);
        if (element)
        {
            element.Pressed();
        }
    }

    private void ResetGame()
    {
        BaseElement[] elements = rootParent.GetComponentsInChildren<BaseElement>();
        elements.ToList().ForEach(obj => Destroy(obj.gameObject));
        
        BaseElement[] elementsInPool = poolParent.GetComponentsInChildren<BaseElement>();
        elementsInPool.ToList().ForEach(obj => Destroy(obj.gameObject));

        timeTick = 0f;
        pool.Clear();
    }
    
    private void Awake()
    {
        gameState = GameState.Wait;
        pool = new Stack<BaseElement>();
        uiManager = GetComponent<GameUIManager>();
        inputListener = GetComponent<KeyInputListener>();
        rootParent = GameObject.Find("ObjectRoot").transform;
        poolParent = GameObject.Find("ObjectPool").transform;
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    private void Start()
    {
        uiManager.SetGameWaiting();
    }

    private void FixedUpdate()
    {
        if (gameState == GameState.Gaming)
        {
            if (rootParent.childCount >= maxEndCount)
            {
                SetGameState(GameState.End);
                return;
            }
            
            timeTick += Time.unscaledDeltaTime;
            uiManager.SetGameTimeing(timeTick);
            
            createDuration -= Time.unscaledDeltaTime;
            if (createDuration <= 0f)
            {
                createDuration = createDurationConst - createRate;
                AddElement();
            }
        }
    }
    
}