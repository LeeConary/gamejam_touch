// Copyright (c) PeroPeroGames Co., Ltd.
// Author: LeeConary
// Created On: 2023-07-08 21:06
// Description:

using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    private AudioSource audioSource;
    
    [SerializeField] private AudioClip[] hitClips;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayHitVoice()
    {
        audioSource.clip = hitClips[Random.Range(0, hitClips.Length)];
        audioSource.Play();
    }
}