// Copyright (c) PeroPeroGames Co., Ltd.
// Author: LeeConary
// Created On: 2023-07-07 21:29
// Description:

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using Random = UnityEngine.Random;

namespace Element
{
    public class BaseElement : MonoBehaviour, IElement
    {
        [SerializeField] private Sprite[] sprNormal;
        [SerializeField] private Sprite[] sprPressed;
        
        private SpriteRenderer sprRenderer;
        private ElementHandler handler;

        private List<Tweener> tweeners;
        private Camera uiCamera;

        private Coroutine stayCoroutine;
        
        public TouchElementType elementType;
        public TouchElementState elementState;
        
        private void Awake()
        {
            sprRenderer = GetComponent<SpriteRenderer>();
            tweeners = new List<Tweener>();
            uiCamera = Camera.allCameras.ToList().Find(camera => camera.name == "UICamera");
        }

        public void Reset(ElementHandler handler)
        {
            this.handler = handler;
            elementType = TouchElementType.Normal;
            ResetViewState();
            ResetPosition();
        }

        private void ResetViewState()
        {
            gameObject.SetActive(true);
            sprRenderer.DOFade(1f, 0f);
            sprRenderer.color = Color.white;
            sprRenderer.sprite = sprNormal[Random.Range(0, sprNormal.Length - 1)];
            transform.localScale = Vector3.zero;
        }

        private void ResetPosition()
        {
            var halfSprWidth = sprRenderer.sprite.texture.width / 2;
            var halfSprHeight = sprRenderer.sprite.texture.height / 2;
            
            var halfWidth = Random.Range(halfSprWidth, Screen.width - halfSprWidth);
            var halfHeight = Random.Range(halfSprHeight, Screen.height - halfSprHeight);
            var appearPos = uiCamera.ScreenToWorldPoint(new Vector3(halfWidth, halfHeight, 0f));
            transform.position = new Vector3(appearPos.x, appearPos.y, 0f);
        }

        public void Show()
        {
            elementState = TouchElementState.Appearing;
            Tweener appearTweener = transform.DOScale(Vector3.one * Random.Range(1.2f, 1.5f), 0.2f).OnComplete((() =>
            {
                elementState = TouchElementState.PerfectOn;
                //sprRenderer.color = Color.green;
                stayCoroutine = StartCoroutine(Stay());
            }));
            appearTweener.Play();
            tweeners.Add(appearTweener);
        }

        private IEnumerator Stay()
        {
            yield return new WaitForSeconds(1f);
            Disappear("Scale");
        }

        public void Pressed()
        {
            if (stayCoroutine != null)
            {
                StopCoroutine(stayCoroutine);
            }

            if (elementState == TouchElementState.PerfectOn)
            {
                handler.audioManager.PlayHitVoice();
                sprRenderer.sprite = sprPressed[Random.Range(0, sprPressed.Length - 1)];
                sprRenderer.color = Color.red;
                Disappear("FadeAndUp");
            }
        }

        public void Disappear()
        {
            throw new System.NotImplementedException();
        }

        public void Disappear(string tweenType)
        {
            KillTweeners();
            elementState = TouchElementState.Disappearing;

            TweenCallback disappear = () =>
            {
                sprRenderer.DOFade(0f, 0f);
                handler.RemoveElement(this);
            };
            switch (tweenType)
            {
                case "Scale":
                    sprRenderer.color = Color.white;
                    transform.DOScale(Vector3.zero, 0.1f).OnComplete(disappear);
                    break;
                case "FadeAndUp":
                    transform.DOLocalMoveY(transform.position.y + 0.1f, 0.2f);
                    sprRenderer.DOFade(0f, 0.2f).OnComplete(disappear);
                    break;
                default:
                    disappear();
                    break;
            }
        }

        private void KillTweeners()
        {
            tweeners.ForEach(tweener =>
            {
                tweener.Kill();
            });
            tweeners.Clear();
        }
    }
}