﻿
public interface IElement
{
    void Reset(ElementHandler handler);

    void Show();

    void Pressed();

    void Disappear();
}
