// Copyright (c) PeroPeroGames Co., Ltd.
// Author: LeeConary
// Created On: 2023-07-08 16:55
// Description:

using System;
using System.Collections;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameUIManager : MonoBehaviour
{
    private ElementHandler handler;
        
    //Waiting UI
    [SerializeField]
    private RectTransform panel;
    
    [SerializeField]
    private Text txtResult;

    [SerializeField] private Text txtTip;

    //Gaming UI
    [SerializeField]
    private Text txtTiming;

    private float gameTiming;

    public void SetGameTimeing(float timeValue)
    {
        txtTiming.text = timeValue.ToString("00.000");
        gameTiming = timeValue;
    }

    public void SetGameWaiting()
    {
        txtResult.gameObject.SetActive(true);
        txtTip.gameObject.SetActive(true);
    }

    public void SetGameStart()
    {
        txtTip.gameObject.SetActive(false);
        panel.DOLocalMoveY(1000f, 0.5f).OnComplete(() =>
        {
            txtTiming.gameObject.SetActive(true);
            handler.SetGameState(GameState.Gaming);
        });
    }

    public void SetGameEnd()
    {
        txtResult.text = gameTiming.ToString("00.000");
        txtResult.gameObject.SetActive(true);
        
        txtTiming.gameObject.SetActive(false);
        panel.DOLocalMoveY(0f, 0.5f).OnComplete(() =>
        {
            StartCoroutine(EndToDelay());
        });
    }

    private IEnumerator EndToDelay()
    {
        yield return new WaitForSeconds(2f);
        handler.SetGameState(GameState.Wait);
    }

    private void Awake()
    {
        handler = GetComponent<ElementHandler>();
    }
}